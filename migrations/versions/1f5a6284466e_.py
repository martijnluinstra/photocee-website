"""empty message

Revision ID: 1f5a6284466e
Revises: 5c0cade9aa44
Create Date: 2016-09-04 23:12:23.453000

"""

# revision identifiers, used by Alembic.
revision = '1f5a6284466e'
down_revision = '5c0cade9aa44'

from alembic import op
import sqlalchemy as sa


def upgrade():
    ### commands auto generated by Alembic - please adjust! ###
    op.create_table('wall_of_fame_photo',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('photo_name', sa.String(length=256), nullable=False),
    sa.Column('date', sa.DateTime(), nullable=False),
    sa.Column('category', sa.String(length=256), nullable=True),
    sa.Column('photographer', sa.String(length=256), nullable=False),
    sa.Column('photographer_cover_id', sa.Integer(), nullable=True),
    sa.Column('description', sa.String(length=2048), nullable=True),
    sa.Column('width', sa.Integer(), nullable=False),
    sa.Column('height', sa.Integer(), nullable=False),
    sa.PrimaryKeyConstraint('id')
    )
    ### end Alembic commands ###


def downgrade():
    ### commands auto generated by Alembic - please adjust! ###
    op.drop_table('wall_of_fame_photo')
    ### end Alembic commands ###
