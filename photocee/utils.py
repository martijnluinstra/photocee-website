from datetime import datetime

from flask import request, redirect, abort

from urlparse import urlparse, urlunparse, urljoin
from werkzeug.urls import url_decode, url_encode

from . import app

@app.template_test()
def upcoming(activity):
    return activity.date >= datetime.now()


def url_inject_args(url, args):
    parts = list(urlparse(url))
    md = url_decode(parts[4])
    md.update(args)
    parts[4] = url_encode(md, sort=True)
    return urlunparse(parts)


def _current_page(urls=None, prefixes=None, return_value='active', **kwargs):
    matches = False
    if urls:
        matches = matches or _current_url_in(urls, **kwargs)
    if prefixes:
        matches = matches or _current_url_starts_with(prefixes, **kwargs)
    return return_value if matches else ''


def _current_url_in(urls, **kwargs):
    for url in urls:
        if request.path == url:
            return True
    return False


def _current_url_starts_with(prefixes, **kwargs):
    url = request.path
    for prefix in prefixes:
        if url.startswith(prefix) or url.startswith('/%s' % (prefix)):
            return True
    return False


@app.context_processor
def utility_processor():
    def cover_profile_link(cover_id):
        return 'https://www.svcover.nl/profiel.php?lid=%d' % (cover_id)
    return dict(
        current_page=_current_page,
        cover_profile_link=cover_profile_link,
        url_inject_args = url_inject_args,
    )


def is_safe_url(target):
    ref_url = urlparse(request.host_url)
    test_url = urlparse(urljoin(request.host_url, target))
    return test_url.scheme in ('http', 'https') and \
           ref_url.netloc == test_url.netloc

def redirect_or_next(destination):
    next_url = request.args.get('next')
    if not is_safe_url(next_url):
        return abort(400)
    return redirect(next_url or destination)

def url_for_or_next(view):
    next_url = request.args.get('next')
    if not is_safe_url(next_url):
        return abort(400)
    return next_url or url_for(view)

