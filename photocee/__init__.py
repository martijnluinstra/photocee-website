from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_wtf.csrf import CsrfProtect
import flask_uploads
from flask_coverapi import CoverSessionManager

# Init app
app = Flask(__name__)
app.config.from_object('config')

db = SQLAlchemy(app)
migrate = Migrate(app, db)

session_manager = CoverSessionManager(app.config['COVER_APP'], app.config['COVER_SECRET'], flask_app=app)

photos_uploadset = flask_uploads.UploadSet('photos', flask_uploads.IMAGES)
flask_uploads.configure_uploads(app, (photos_uploadset,))

CsrfProtect(app)

from .planner import planner, models
from .wall_of_fame import wall_of_fame, models
from .contests import contests, models

app.register_blueprint(planner)
app.register_blueprint(wall_of_fame)
app.register_blueprint(contests)

from . import views, utils
