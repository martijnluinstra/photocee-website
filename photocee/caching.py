from __future__ import division

import os

from shutil import copyfile
from PIL import Image

from flask import send_file

from . import app, photos_uploadset


def resize_image(filename, destination, width, height=None):
    with Image.open(filename) as im:
        old_width, old_height = im.size
        
        if not height:
            height = width * (old_height/old_width)

        resized = im.resize((int(width), int(height)), Image.BICUBIC)
            
    if width < old_width or height < old_height:
        resized.save(destination)
    else:
        copyfile(filename, destination)



def serve_cached_image(cache, filename, width=None, height=None):
    name, extension = os.path.splitext(filename)
    
    if width and height:
        cached_filename = '{}_{}_{}{}'.format(name, width, height, extension)
    elif width:
        cached_filename = '{}_{}{}'.format(name, width, extension)
    else:
        cached_filename = filename

    cached_path = os.path.join(app.config['CACHE_FOLDER'], cache, name, cached_filename)

    if not os.path.exists(os.path.dirname(cached_path)):
        os.makedirs(os.path.dirname(cached_path))

    if not os.path.isfile(cached_path):
        if not width:
            copyfile(photos_uploadset.path(filename), cached_path)
        else:
            resize_image(photos_uploadset.path(filename), cached_path, width, height)

    return send_file(cached_path)

