var loadDialog = function(e, link) {
	if (e.shiftKey || e.ctrlKey || e.metaKey)
		return;

	e.preventDefault();

	var $link = $(link);
	var $placeholder = $('<div class="modal">').appendTo(document.body);


	if($link.find('i.fa').length){
		var $icon = $link.find('i.fa').first();
		$icon.data('icon', $icon.attr('class'));
		$icon.removeClass();
		$icon.addClass('fa fa-circle-o-notch fa-spin fa-fw');
	}else{
		$link.button('loading');
	}

	switch ($link.data('popup'))
	{
		case 'modal':
			$placeholder.load($link.prop('href') + ' .modal-dialog', function() {
				$placeholder.modal('show');
				$(document.body).trigger(jQuery.Event('dialog-content-loaded', {target: $placeholder.get(0)}));
			});
			break;

		case 'image':
			$placeholder.html('\
				<div class="modal-dialog modal-lg image-modal">\
					<div class="modal-content">\
						<div class="modal-header">\
							<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>\
							<h4 class="modal-title"></h4>\
						</div>\
						<img src="#" class="">\
					</div>\
				</div>');
			$placeholder.find('img').prop('src', $link.prop('href'));
			$placeholder.find('.modal-title').text($link.prop('title'));				
			$placeholder.modal('show');
			break;

		default:
			throw Error("Unknown type '" + $link.data('popup') + "'");
	}

	$placeholder.on('click', '*[data-dismiss=modal]', function(e) {
		e.preventDefault();
		$placeholder.modal('hide');
	});

	$placeholder.on('shown.bs.modal', function() {
		$placeholder.trigger('partial-ready');
	});

	// Remove dialog from HTML when hidden
	$placeholder.on('hidden.bs.modal', function() {
		if($link.find('i.fa').length){
			var $icon = $link.find('i.fa').first();
			$icon.removeClass();
			$icon.addClass($icon.data('icon'));
		}else{
			$link.button('reset');
		}
		$placeholder.remove();
	});
};

$(document).on('click', 'a[data-popup]', function(e){
	loadDialog(e, this);
});
