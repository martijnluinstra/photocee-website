(function ( $ ) {
    $.fn.autocompleteAlmanac = function(options) {
        var settings = $.extend({
            dataURL: '/almanakproxy',
            coverProfilePhotoURL: 'https://www.svcover.nl/foto.php',
            typeaheadOptions: {
                highlight: true
            },
        }, options );

        var members = new Bloodhound({ 
            datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'), 
            queryTokenizer: Bloodhound.tokenizers.whitespace, 
            identify: function(obj) { return obj.id; },
            remote: {
                url: settings.dataURL + '?search=%QUERY',
                wildcard: '%QUERY'
            }
        });

        var format_suggestion = function(member){
            return '<div>'+
                    '<img src="' + settings.coverProfilePhotoURL +'?lid_id=' + member.id + '&format=square&width=60" alt="'+ member.name +'">'+
                    '<strong>'+ member.name +'</strong> ' + 
                    '(' + member.starting_year + ')' +
                '</div>';
        };

        return this.each(function() {
            $(this).typeahead(settings.typeaheadOptions, { 
                name: 'members',
                display: 'name',
                source: members,
                templates: {
                    suggestion: format_suggestion
                }
            });

            $(this).bind('typeahead:select typeahead:autocomplete', function(ev, suggestion) {
                if ( settings.hasOwnProperty('idField') ) {
                    $(settings.idField).val(suggestion.id);
                } else {
                    $('#'+$(this).attr('id')+'_cover_id').val(suggestion.id);
                }
            });
        });
    };
}( jQuery ));

$(document).on('ready dialog-content-loaded', function(e){
    $(e.target).find('.autocomplete-almanac').autocompleteAlmanac();
});
