var csrfToken = $('meta[name=csrf-token]').attr('content')

$.ajaxSetup({
    beforeSend: function(xhr, settings) {
        if (!/^(GET|HEAD|OPTIONS|TRACE)$/i.test(settings.type) && !this.crossDomain) {
            xhr.setRequestHeader("X-CSRFToken", csrfToken)
        }
    }
})

var updatePhotographerIcon = function(activity_id, status, status_descriptor, activity_container){
    var icon = activity_container.find('.photographer[data-photographer-id='+ window.cover_user_id+']');
    if(icon.length === 0){
        icon = $('<div></div>',{
            'class': 'photographer status-'+status_descriptor,
            'title': window.cover_user_name+': '+window.status_options[status],
            'data-toggle': 'tooltip',
            'data-placement': 'bottom',
            'data-photographer-id': window.cover_user_id,
        });
        icon.append($('<img>').attr('src', 'https://www.svcover.nl/foto.php?lid_id='+window.cover_user_id+'&format=square&width=60'));
        icon.appendTo(activity_container.find('.photographers.desktop-alternative'));
    }else{
        icon.removeClass().addClass('photographer').addClass('status-'+status_descriptor);
    }
};

var updatePhotographerCounters = function(activity_id, status, status_descriptor, activity_container){
    var previous_counter = activity_container.find('.photographers.mobile-alternative .label-' + activity_container.data('my-status'));
    previous_counter.text(parseInt(previous_counter.text())-1);
    
    var current_counter = activity_container.find('.photographers.mobile-alternative .label-'+status_descriptor);
    current_counter.text(parseInt(current_counter.text())+1);
}

var submitStatus = function(activity_id, status){
    var form_data = {};
    form_data['status-'+activity_id] = status
    $.post('/planner/status',form_data, function() {
        var status_descriptor = window.status_options[status].replace(/\s+/g, '-').toLowerCase()
        var activity_container = $('#status-form tr[data-activity-id='+activity_id+']');

        updatePhotographerIcon(activity_id, status, status_descriptor, activity_container);
        updatePhotographerCounters(activity_id, status, status_descriptor, activity_container);
        activity_container.data('my-status', status_descriptor);
        // activity_container.removeClass().addClass('status').addClass('status-'+status_descriptor);
    }).fail(function(response) {
        alert( "Something went wrong, try again!");
    });
};

$('[data-toggle="tooltip"]').tooltip();

$('#submit-status-form').hide();

$('#status-form input:radio').change(function(){
    var activity_id = $(this).closest('tr').data('activity-id');
    var status = $('#status-form input[name=' + $(this).prop('name') + ']:checked').val();
    submitStatus(activity_id, status);
});

$('#status-form select').change(function(){
    var activity_id = $(this).closest('tr').data('activity-id');
    var status = $(this).find('option:selected').first().val();
    submitStatus(activity_id, status);
});


var removePhotographerID = function(id) {
    var ids = $('#photographer-filter-ids').val().split(',');
    var index = ids.indexOf(id);
    if (index > -1) {
        ids.splice(index, 1);
    }
    $('#photographer-filter-ids').val(ids.join());  
};


var createPhotographerLabel = function(photographer) {
    var $closebutton = $('<button type="button" class="close" data-dismiss="label" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
    $closebutton.click(function(evt){
        var $parent = $(this).closest('.label');
        removePhotographerID($parent.data('member-id').toString());
        $parent.tooltip('destroy');
        $parent.remove();
    });

    var $label = $('<span>').addClass('label label-default').data({'toggle': 'tooltip', 'placement': 'bottom'});
    $label.data('member-id', photographer.id).attr('title', photographer.name);    
    $label.text(photographer.first_name).append(" ").append($closebutton);

    $('#photographer-filter-display').append(" ").append($label);
    $('#photographer-filter-display .label').tooltip();
};


if ($('#photographer-filter-ids').val().length > 0) {
    $.getJSON('/almanakreverse?search='+$('#photographer-filter-ids').val(), function(data){
        $('#photographer-filter-display').empty();
        data.forEach(createPhotographerLabel);
    }).fail(function(){
       $('#photographer-filter-display').html('<span class="text-danger">Selected members could not be found!</span>'); 
    });
}


$('#photographer-filter').on('typeahead:select typeahead:autocomplete', function(evt, suggestion) {
    evt.preventDefault();
    if($('#photographer-filter-ids').val() !== ''){
        var ids = $('#photographer-filter-ids').val().split(',');
        ids.push(suggestion.id);
        $('#photographer-filter-ids').val(ids.join());
    }else{
        $('#photographer-filter-ids').val(suggestion.id);
    }
    createPhotographerLabel(suggestion);
    $('#photographer-filter').typeahead('val', '');
});


$('#clear-filter').click(function(evt){
    evt.preventDefault();
    $('#filter-form').find(':checkbox').prop('checked', false);
    $('#filter-form').find('[name=status] option').prop('selected', false);
    $('#photographer-filter').typeahead('val', '');
    $('#photographer-filter-ids').val('');
    $('#photographer-filter-display').empty();
    $('#filter-form').submit();
});
