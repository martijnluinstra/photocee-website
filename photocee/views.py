import requests

from flask import render_template, request, jsonify
from flask_coverapi import admin_required, current_user, CoverSession

from . import app

@app.route('/', methods=['GET'])
def view_home():
    return render_template('index.html')


@app.route('/almanakproxy', methods=['GET'])
@admin_required
def almanakproxy():
    cover_cookie_name = app.config.get('COVER_COOKIE_NAME', 'cover_session_id')
    almanak_url = app.config.get('COVER_ALMANAK_URL', 'https://www.svcover.nl/almanak.php')
    params = dict(request.args)
    if request.cookies.get(cover_cookie_name):
        params['session_id'] = request.cookies.get(cover_cookie_name)
    response = requests.get(almanak_url, params=params, headers={'accept': 'application/json, text/javascript'})
    response.raise_for_status()
    return response.text


@app.route('/almanakreverse', methods=['GET'])
@admin_required
def almanakreverse():
    query = request.args.get('search', '').split(',')
    def generate(data):
        for member_id in data:
            print member_id
            response = app.cover_session_manager.request_json({
                'method': 'get_member',
                'session_id': current_user.session_id,
                'member_id': member_id
            })

            if not 'result' in response:
                continue

            photographer = CoverSession(current_user.api, current_user.session_id, response['result'])
            yield {
                'id': photographer.get_id(),
                'name': photographer.full_name,
                'first_name': photographer.user['voornaam']
            }
    return jsonify(list(generate(query)))
