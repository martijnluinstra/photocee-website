from flask_wtf import Form
from wtforms import StringField, IntegerField, DateTimeField, SelectField, validators

from models import PHOTOGRAPHER_STATUS_OPTIONS


class ActivityForm(Form):
    name = StringField('Name', validators=[
        validators.InputRequired(message='Name is required')
        ])
    date  = DateTimeField('Date and time', format='%Y-%m-%d %H:%M', validators=[
        validators.InputRequired(message='Date is required')
        ], render_kw={"placeholder": "1993-09-20 15:00"})
    location = StringField('Location', validators=[
        validators.Optional(strip_whitespace=True)
        ], render_kw={"placeholder": "Optional"})
    description = StringField('Description', validators=[
        validators.Optional(strip_whitespace=True)
        ], render_kw={"placeholder": "Optional"})
    cover_id = IntegerField('Cover ID', validators=[
        validators.Optional()
        ], render_kw={"placeholder": "Optional"})


class PhotographerForm(Form):
    photographer = StringField('Photographer', validators=[
        validators.InputRequired(message='Photographer is required'),
        validators.Length(max=256, message='Maximum of 256 characters allowed')
        ])
    photographer_cover_id = IntegerField('Photographer Cover ID', validators=[
        validators.InputRequired(message='Photographer should have a Cover member ID')
        ])
    status = SelectField('Status',  choices=PHOTOGRAPHER_STATUS_OPTIONS, validators=
        [validators.InputRequired(message='Status is required')
        ])
