from photocee import db, app
from sqlalchemy_utils.types.choice import ChoiceType

import urllib

PHOTOGRAPHER_STATUS_OPTIONS = [
        (u'0', u'Yes'),
        (u'1', u'Uploaded'),
        (u'2', u'Unfinished photos folder'),
        (u'3', u'Probably'),
        (u'4', u'Preferably not'),
        (u'5', u'No')
    ]


class Activity(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    cover_id = db.Column(db.Integer, nullable=True)
    name = db.Column(db.String(255), nullable=False)
    description = db.Column(db.String(255), nullable=True)
    location = db.Column(db.String(255), nullable=True)
    date = db.Column(db.DateTime, nullable=False)
    published = db.Column(db.Boolean, nullable=False, default=False)
    archived = db.Column(db.Boolean, nullable=False, default=False)
    photographers = db.relationship('Photographer', backref='activity', lazy='select')

    @property
    def cover_url(self):
        return '%s?%s' %(app.config.get('COVER_AGENDA_URL'), urllib.urlencode(dict(agenda_id=self.cover_id)))


class Photographer(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    activity_id = db.Column(db.Integer, db.ForeignKey('activity.id'), nullable=False)
    cover_id = db.Column(db.Integer, nullable=False)
    name = db.Column(db.String(255), nullable=False)
    status = db.Column(ChoiceType(PHOTOGRAPHER_STATUS_OPTIONS))
