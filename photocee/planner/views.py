from __future__ import division

from flask import request, render_template, redirect, url_for, abort, flash, get_flashed_messages, current_app
from flask_coverapi import admin_required, current_user, CoverSession

from sqlalchemy.orm import Load
from sqlalchemy.sql import func

from urlparse import urlparse, urljoin
from datetime import datetime

import sqlalchemy_utils
import json

from photocee import db
from photocee.utils import redirect_or_next, url_for_or_next

from . import planner
from .models import Activity, Photographer, PHOTOGRAPHER_STATUS_OPTIONS
from .forms import ActivityForm, PhotographerForm


def _update_activities():
    activities = current_app.cover_session_manager.request_json({
            'method': 'agenda',
            'session_id': current_user.session_id
        })
    for activity in activities:
        if activity['replacement_for'] is not None:
            # Unapproved activity, skip.
            continue
        saved_activity = Activity.query.filter_by(cover_id=activity['id']).first()
        if not saved_activity:
            saved_activity = Activity(cover_id=int(activity['id']))
            db.session.add(saved_activity)
        saved_activity.name = activity['kop']
        saved_activity.date = datetime.strptime(activity['van'].split('+', 1)[0], '%Y-%m-%d %H:%M:%S')
        saved_activity.location = activity['locatie']
    db.session.commit()


def _apply_filters(query):
    filters = {}

    if request.args.get('archived') == 'None':
        archived = None
    else:
        archived = request.args.get('archived') == 'True'

    if archived is not None:
        query = query.filter(Activity.archived == archived)

    if request.args.get('published') and request.args.get('published') != 'None':
        filters['published'] = request.args.get('published') == 'True'
        query = query.filter(Activity.published == filters['published'])

    if request.args.get('status') or request.args.get('photographers'):
        photographers_query = db.session.query(Photographer.activity_id)

        if request.args.get('photographers'):
            filters['photographers'] = request.args.get('photographers')
            photographers = request.args.get('photographers').split(',')
            photographers_query = photographers_query.filter(Photographer.cover_id.in_(photographers))

        if request.args.get('status'):
            filters['status'] = request.args.getlist('status')
            photographers_query = photographers_query.filter(Photographer.status.in_(filters['status']))

        query = query.filter(Activity.id.in_(photographers_query.subquery()))

    if filters or request.args.get('archived'):
        filters['archived'] = archived

    return query, filters


@planner.route('/', methods=['GET'])
@admin_required
def main():
    _update_activities()
    
    my_activities_subq = Photographer.query.filter_by(cover_id=current_user.get_id()).subquery()
    activities_query = db.session.query(Activity, my_activities_subq.c.status.label('my_status')).outerjoin(my_activities_subq, Activity.id==my_activities_subq.c.activity_id)
    
    backlog = db.session.query(func.count(Activity.id))\
                .outerjoin(my_activities_subq, Activity.id==my_activities_subq.c.activity_id)\
                .filter(my_activities_subq.c.cover_id == current_user.get_id())\
                .filter(my_activities_subq.c.status == u'0')\
                .filter(Activity.archived==False)\
                .filter(Activity.date < datetime.now())\
                .scalar()

    #filters
    activities_query, filters = _apply_filters(activities_query)

    activities = activities_query.order_by(Activity.date.desc()).all()

    return render_template('planner/planner.html', 
                           activities=activities, 
                           status_options=dict(PHOTOGRAPHER_STATUS_OPTIONS),
                           status_options_json=json.dumps(dict(PHOTOGRAPHER_STATUS_OPTIONS)), 
                           filters=filters,
                           backlog=backlog)


@planner.route('/status', methods=['POST'])
@admin_required
def update_status():
    photographer = current_user
    for key, value in request.form.iteritems():
        fieldname = key.split('-', 1)
        if not fieldname[0] == 'status':
            continue

        activity_id = fieldname[-1]
        activity = Activity.query.get(activity_id)
        if activity:
            add_or_update_photographer(photographer, activity, value)
    db.session.commit()
    return redirect_or_next(url_for('planner.main'))


def add_or_update_photographer(photographer, activity, status):
    saved_photographer = Photographer.query.filter_by(activity_id=activity.id).filter_by(cover_id=photographer.get_id()).first()
    if not saved_photographer:
        saved_photographer = Photographer(activity_id=activity.id, cover_id=photographer.get_id(), name=photographer.full_name, status=status)
        db.session.add(saved_photographer)
    else:
        saved_photographer.status = status
    auto_archive(activity)


def auto_archive(activity):
    if not activity.published:
        return
    db.session.commit()
    if not Photographer.query.filter_by(activity_id=activity.id).filter(Photographer.status.in_([u'0', u'2', u'3'])).first():
        activity.archived = True
    else: 
        activity.archived = False


@planner.route('/activity/<int:activity_id>/status/add', methods=['GET', 'POST'])
@admin_required
def add_status_activity(activity_id):
    activity = Activity.query.get_or_404(activity_id)
    form = PhotographerForm()
    if form.validate_on_submit():
        response = current_app.cover_session_manager.request_json({
            'method': 'get_member',
            'session_id': current_user.session_id,
            'member_id': form.photographer_cover_id.data
        })
        if 'result' in response:
            photographer = CoverSession(current_user.api, current_user.session_id, response['result'])
        else:
            return "Invalid Cover Member ID", 400
        add_or_update_photographer(photographer, activity, form.status.data)
        db.session.commit()
        return redirect_or_next(url_for('planner.main'))
    return render_template('planner/photographer_form.html', form=form, activity=activity, meta={
        'name': 'Add photographer',
        'cancel_url': url_for_or_next('planner.main')
    })


@planner.route('/<int:activity_id>/archive', methods=['GET'])
@admin_required
def archive_activity(activity_id):
    activity = Activity.query.get_or_404(activity_id)
    activity.archived = request.args.get('archive') != 'False'
    db.session.commit()
    return redirect_or_next(url_for('planner.main'))


@planner.route('/<int:activity_id>/publish', methods=['GET'])
@admin_required
def publish_activity(activity_id):
    activity = Activity.query.get_or_404(activity_id)
    activity.published = request.args.get('publish') != 'False'
    auto_archive(activity)
    db.session.commit()
    return redirect_or_next(url_for('planner.main'))


@planner.route('/activity/add', methods=['GET', 'POST'])
@admin_required
def add_activity():
    """ Try to create a new activity. """
    form = ActivityForm()
    if form.validate_on_submit():
        activity = Activity(
            name=form.name.data, description=form.description.data, location=form.location.data, date=form.date.data, cover_id=form.cover_id.data)
        db.session.add(activity)
        db.session.commit()
        return redirect(url_for('planner.main'))
    return render_template('form_dialog.html', form=form, meta={
        'name': 'Add activity',
        'cancel_url': url_for('planner.main')
    })



@planner.route('/activity/<int:activity_id>', methods=['GET', 'POST'])
@admin_required
def edit_activity(activity_id):
    activity = Activity.query.get_or_404(activity_id)
    form = ActivityForm(request.form, activity)
    if form.validate_on_submit():
        form.populate_obj(activity)
        db.session.commit()
        return redirect_or_next(url_for('planner.main'))
    return render_template('form_dialog.html', form=form, meta={
        'name': 'Edit activity',
        'cancel_url': url_for_or_next('planner.main')
    })
