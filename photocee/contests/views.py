from PIL import Image

from flask import render_template, url_for, redirect, request
from flask_coverapi import admin_required, login_required, current_user

from photocee import db, photos_uploadset
from photocee.caching import serve_cached_image

from . import contests
from .models import Poll, PollOption, PollVote, PollVoteRegistered
from .forms import PollForm, PollOptionForm, PollVoteForm
from .utils import randomize_for_user, get_poll_option_index, get_previous_poll_option_id, get_next_poll_option_id


@contests.route('/', methods=['GET'])
def main():
    view = request.args.get('view', 'default')

    if not current_user.is_anonymous:
        cover_id = current_user.get_id()
    else:
        cover_id = 0
    votes = PollVoteRegistered.query.filter_by(cover_id=cover_id).all()

    if view == 'archived':
        polls = Poll.query.order_by(Poll.deadline.desc()).all()
    else:
        polls = Poll.query.filter_by(archived=False).order_by(Poll.deadline.desc()).all()

    return render_template('contests/main.html', view=view, polls=polls, votes=[v.poll_id for v in votes])


@contests.route('/polls/add', methods=['GET','POST'])
@admin_required
def polls_add():
    form = PollForm()
    if form.validate_on_submit():
        poll = Poll(
            name=form.name.data,
            description=form.description.data,
            deadline=form.deadline.data,
            archived=form.archived.data,
        )
        db.session.add(poll)
        db.session.commit()
        return redirect(url_for('contests.polls_view', poll_id=poll.id))
    return render_template('form_dialog.html', form=form, meta={
        'name': 'Add poll',
        'cancel_url': url_for('contests.main')
    })


@contests.route('/polls/<int:poll_id>', methods=['GET'])
@admin_required
def polls_view(poll_id):
    poll = Poll.query.get_or_404(poll_id)
    results = []
    for idx, option in enumerate(poll.options):
        votes = option.votes.all()
        results.append({
            'option_id': option.id,
            'display': 'Photo {}'.format(idx+1),
            'votes': len(votes)
            })
    number_votes = sum([r['votes'] for r in results])
    form = PollOptionForm()
    return render_template('contests/poll.html', 
        poll=poll, 
        results=results, 
        number_votes=number_votes,
        form=form
        )


@contests.route('/polls/<int:poll_id>/vote', methods=['GET', 'POST'])
@login_required
def polls_vote(poll_id):
    poll = Poll.query.get_or_404(poll_id)
    form = PollVoteForm()
    form.choice.choices = [(o.id, "Photo {}".format(idx+1)) for idx, o in enumerate(randomize_for_user(poll.options))]
    if form.validate_on_submit():
        vote = PollVote(
            position=1,
            poll_option_id=form.choice.data,
            )
        registered = PollVoteRegistered(
            cover_id=current_user.get_id(),
            poll_id=poll.id
            )
        db.session.add(vote)
        db.session.add(registered)
        db.session.commit()
        return redirect(url_for('contests.main'))
    return render_template('form_dialog.html', form=form, meta={
        'name': 'Choose your favourite photo!',
        'cancel_url': url_for('contests.main')
    })


@contests.route('/polls/<int:poll_id>/edit', methods=['GET','POST'])
@admin_required
def polls_edit(poll_id):
    poll = Poll.query.get_or_404(poll_id)
    form = PollForm(request.form, poll)
    if form.validate_on_submit():
        form.populate_obj(poll)
        db.session.commit()
        return redirect(url_for('contests.polls_view', poll_id=poll.id))
    return render_template('form_dialog.html', form=form, meta={
        'name': 'Edit poll',
        'cancel_url': url_for('contests.polls_view', poll_id=poll.id)
    })


@contests.route('/polls/<int:poll_id>/options/add', methods=['GET', 'POST'])
@admin_required
def poll_options_add(poll_id):
    poll = Poll.query.get_or_404(poll_id)
    form = PollOptionForm()
    if form.validate_on_submit():
        filename = photos_uploadset.save(form.photo.data)
        with Image.open(photos_uploadset.path(filename)) as im:
            width, height = im.size
        poll_option = PollOption(
            description=form.description.data,
            photo_name=filename,
            width=width,
            height=height,
            poll=poll
        )
        db.session.add(poll_option)
        db.session.commit()
        return redirect(url_for('contests.polls_view', poll_id=poll.id))
    return render_template('form_dialog.html', form=form, meta={
        'name': 'Add option to {}'.format(poll.name),
        'cancel_url': url_for('contests.polls_view', poll_id=poll.id),
        'form_kwargs': {'upload': True}
    })


@contests.route('/polls/options/<int:poll_option_id>', methods=['GET'])
def poll_options_view(poll_option_id):
    poll_option = PollOption.query.get_or_404(poll_option_id)
    return render_template(
        'contests/poll_option.html',
        poll_option=poll_option,
        current_index=get_poll_option_index(poll_option.poll.options, poll_option),
        previous_id=get_previous_poll_option_id(poll_option.poll.options, poll_option),
        next_id=get_next_poll_option_id(poll_option.poll.options, poll_option)
    )


@contests.route('/polls/options/<int:poll_option_id>/photo', methods=['GET'])
def poll_options_photo(poll_option_id):
    width = request.args.get('width', None)
    height = request.args.get('height', None)
    if width and width.strip():
        width = int(float(width))
    if height and height.strip():
        height = int(float(height))
    poll_option = PollOption.query.get_or_404(poll_option_id)
    return serve_cached_image('photos', poll_option.photo_name, width, height)


@contests.route('/polls/options/<int:poll_option_id>/delete', methods=['GET', 'POST'])
@admin_required
def poll_options_delete(poll_option_id):
    poll_option = PollOption.query.get_or_404(poll_option_id)
    if request.method == 'POST':
        db.session.delete(poll_option)
        db.session.commit()
        return redirect(url_for('contests.polls_view', poll_id=poll_option.poll_id))
    return render_template('contests/poll_option_delete_form.html', poll_option=poll_option, meta={
        'name': 'Delete photo',
        'cancel_url': url_for('contests.polls_view', poll_id=poll_option.poll_id)
    })
