from flask_wtf import Form
from flask_wtf.file import FileField, FileAllowed, FileRequired
from wtforms import StringField, DateTimeField, BooleanField, TextAreaField, RadioField, validators

from photocee import photos_uploadset


class PollForm(Form):
    deadline = DateTimeField('Deadline', format='%Y-%m-%d %H:%M', validators=[
        validators.InputRequired(message='Deadline is required')
        ], render_kw={"placeholder": "1993-09-20 15:00"})
    name = StringField('Name', validators=[
        validators.InputRequired(message='Name is required')
        ])
    description = StringField('Description', validators=[
        validators.Optional(strip_whitespace=True),
        validators.Length(max=2048, message='Maximum of 2048 characters allowed')
        ])
    archived = BooleanField('Archived')


class PollOptionForm(Form):
    photo = FileField('Photo', validators=[
        FileRequired('Photo is required'),
        FileAllowed(photos_uploadset, 'File extension not allowed!')
        ])
    description = TextAreaField('Description (optional)', validators=[
        validators.Optional(strip_whitespace=True),
        validators.Length(max=2048, message='Maximum of 2048 characters allowed')
        ])


class PollVoteForm(Form):
    choice = RadioField('Choose a photo', coerce=int, validators=[
        validators.InputRequired(message='Vote is required')
        ])
