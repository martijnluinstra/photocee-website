import random
from datetime import datetime

from flask import request
from flask_coverapi import current_user

from . import contests
from .models import Poll

def seed_randomizer():
    if  current_user.is_anonymous:
        random.seed(request.remote_addr)
    else:
        random.seed(current_user.full_name)


@contests.app_template_test('expired')
def poll_has_expired(poll):
    return poll.deadline < datetime.now()


@contests.app_template_filter('randomize_for_user')
def randomize_for_user(l):
    return _randomize_for_user(l[:])


def _randomize_for_user(l):
    seed_randomizer()
    random.shuffle(l)
    return l


def get_poll_option_index(options, current):
    shuffled = randomize_for_user(options)
    return shuffled.index(current)


def get_poll_option_id_for_index(options, idx):
    shuffled = randomize_for_user(options)
    if idx < 0 or idx >= len(shuffled):
        return None
    return shuffled[idx].id


def get_previous_poll_option_id(options, current):
    idx = get_poll_option_index(options, current)
    return get_poll_option_id_for_index(options, idx-1)


def get_next_poll_option_id(options, current):
    idx = get_poll_option_index(options, current)
    return get_poll_option_id_for_index(options, idx+1)
