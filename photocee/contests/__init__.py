from flask import Blueprint

contests = Blueprint('contests', __name__, url_prefix='/contests')

from . import views, utils
