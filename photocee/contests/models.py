from datetime import datetime

from photocee import db


class Poll(db.Model):
    id = db.Column(db.Integer(), primary_key=True)
    name = db.Column(db.String(255), nullable=False)
    description = db.Column(db.String(2048), nullable=True)
    deadline = db.Column(db.DateTime(), nullable=False)
    archived = db.Column(db.Boolean(), nullable=False, default=False)
    options = db.relationship('PollOption', backref='poll')


class PollOption(db.Model):
    id = db.Column(db.Integer(), primary_key=True)
    description = db.Column(db.String(2048), nullable=True)
    photo_name = db.Column(db.String(256), nullable=False)
    width = db.Column(db.Integer(), nullable=False)
    height = db.Column(db.Integer(), nullable=False)
    poll_id = db.Column(db.Integer(), db.ForeignKey('poll.id'), nullable=False)


class PollVote(db.Model):
    id = db.Column(db.Integer(), primary_key=True)
    position = db.Column(db.Integer(), nullable=False)
    timestamp = db.Column(db.DateTime(), nullable=False)
    poll_option_id = db.Column(db.Integer(), db.ForeignKey('poll_option.id'), nullable=False)
    poll_option = db.relationship('PollOption', backref=db.backref('votes', lazy='dynamic'))
    
    def __init__(self, **kwargs):
        self.timestamp = datetime.now()
        super(PollVote, self).__init__(**kwargs)


class PollVoteRegistered(db.Model):
    id = db.Column(db.Integer(), primary_key=True)
    cover_id = db.Column(db.Integer(), nullable=False)
    poll_id = db.Column(db.Integer(), db.ForeignKey('poll.id'), nullable=False)
    poll = db.relationship('Poll')
