from photocee import db

class Photo(db.Model):
    __tablename__ = 'wall_of_fame_photo'
    id = db.Column(db.Integer(), primary_key=True)
    photo_name = db.Column(db.String(256), nullable=False)
    date = db.Column(db.DateTime, nullable=False)
    category = db.Column(db.String(256), nullable=True)
    photographer = db.Column(db.String(256), nullable=False)
    photographer_cover_id = db.Column(db.Integer, nullable=True)
    description = db.Column(db.String(2048), nullable=True)
    width = db.Column(db.Integer(), nullable=False)
    height = db.Column(db.Integer(), nullable=False)
