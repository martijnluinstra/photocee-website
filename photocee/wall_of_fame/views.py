from PIL import Image

from flask import render_template, url_for, redirect, request
from flask_coverapi import admin_required, current_user

from photocee import db, photos_uploadset
from photocee.caching import serve_cached_image

from . import wall_of_fame
from .models import Photo
from .forms import PhotoForm


@wall_of_fame.route('/', methods=['GET'])
def main():
    photos = Photo.query.order_by(Photo.date.desc()).all()
    return render_template('wall_of_fame/main.html', photos=photos)


@wall_of_fame.route('/photos/add', methods=['GET', 'POST'])
@admin_required
def photos_add():
    form = PhotoForm()
    if form.validate_on_submit():
        filename = photos_uploadset.save(form.photo.data)
        with Image.open(photos_uploadset.path(filename)) as im:
            width, height = im.size
        photo = Photo(
            photo_name=filename,
            date=form.date.data,
            category=form.category.data,
            photographer=form.photographer.data,
            photographer_cover_id=form.photographer_cover_id.data,
            description=form.description.data,
            width=width,
            height=height
        )
        db.session.add(photo)
        db.session.commit()
        return redirect(url_for('wall_of_fame.main'))
    return render_template('wall_of_fame/photo_form.html', form=form, meta={
        'name': 'Add photo',
        'cancel_url': url_for('wall_of_fame.main'),
        'upload': True
    })


@wall_of_fame.route('/photos/<int:photo_id>', methods=['GET'])
def photos_view(photo_id):
    photo = Photo.query.get_or_404(photo_id)
    return render_template('wall_of_fame/photo.html', photo=photo)


@wall_of_fame.route('/photos/<int:photo_id>/photo', methods=['GET'])
def photos_photo(photo_id):
    width = request.args.get('width', None)
    height = request.args.get('height', None)
    if width and width.strip():
        width = int(float(width))
    if height and height.strip():
        height = int(float(height))
    photo = Photo.query.get_or_404(photo_id)
    return serve_cached_image('photos', photo.photo_name, width, height)


@wall_of_fame.route('/photos/<int:photo_id>/edit', methods=['GET', 'POST'])
@admin_required
def photos_edit(photo_id):
    photo = Photo.query.get_or_404(photo_id)
    form = PhotoForm(request.form, photo)
    del form.photo
    if form.validate_on_submit():
        form.populate_obj(photo)
        db.session.commit()
        return redirect(url_for('wall_of_fame.main'))
    return render_template('wall_of_fame/photo_form.html', form=form, meta={
        'name': 'Edit photo',
        'cancel_url': url_for('wall_of_fame.main')
    })


@wall_of_fame.route('/photos/<int:photo_id>/delete', methods=['GET', 'POST'])
@admin_required
def photos_delete(photo_id):
    photo = Photo.query.get_or_404(photo_id)
    if request.method == 'POST':
        db.session.delete(photo)
        db.session.commit()
        return redirect(url_for('wall_of_fame.main'))
    return render_template('wall_of_fame/photo_delete_form.html', photo=photo, meta={
        'name': 'Delete photo',
        'cancel_url': url_for('wall_of_fame.main')
    })
