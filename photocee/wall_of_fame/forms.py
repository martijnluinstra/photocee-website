from flask_wtf import Form
from flask_wtf.file import FileField, FileAllowed, FileRequired
from wtforms import StringField, DateField, IntegerField, TextAreaField, validators

from photocee import photos_uploadset

class PhotoForm(Form):
    date = DateField('Date', format='%Y-%m-%d', validators=[
        validators.InputRequired(message='Name is required')    
    ])
    category = StringField('Category', validators=[
        validators.Optional(strip_whitespace=True),
        validators.Length(max=256, message='Maximum of 256 characters allowed')
    ])
    photographer = StringField('Photographer', validators=[
        validators.InputRequired(message='Photographer is required'),
        validators.Length(max=256, message='Maximum of 256 characters allowed')
    ])
    photographer_cover_id = IntegerField('Photographer Cover ID', validators=[
        validators.Optional(strip_whitespace=True)
    ])
    description = TextAreaField('Description', validators=[
        validators.Optional(strip_whitespace=True),
        validators.Length(max=2048, message='Maximum of 2048 characters allowed')
    ])
    photo = FileField('Photo', validators=[
        FileRequired('Photo is required'),
        FileAllowed(photos_uploadset, 'File extension not allowed!')
    ])
