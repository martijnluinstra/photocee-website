from flask import Blueprint

wall_of_fame = Blueprint('wall_of_fame', __name__, url_prefix='/wall-of-fame')

from . import views
